---
title: "Safety"
draft: false
weight: 15
---

Complete Streets enhance safety by reducing crashes through improvements for non-motorized forms of transportation (i.e. adding or improving existing pedestrian and bicycle facilities, shortening crosswalks by adding medians, etc.). These efforts promote a street that is not only more accessible to non-automobile modes of transportation but is also safer to navigate. 

{{<image src="Speed and fatality.png" 
  link="/destination"
  alt="Figure shows a diagram of the correlation between speed and pedestrian fatalities"
  attr="Traffic speed and pedestrian fatalities" attrlink="https://smartgrowthamerica.org/what-are-complete-streets/"
  position="right">}}

Additionally, the Complete Streets approach considers all users first, meaning that safety for everyone is prioritized over speed of traffic. Data shows that the speed of traffic is the most effective predictor of death or injury following a crash. [^1]  

By initiating policies (such as Complete Streets) that promote safe network design and encourage lower travel speeds, we can reduce the risk of injury and/or death and ensure the safety of all users. This advances the local Vision Zero policy passed by the City of Urbana in 2020. Vision Zero aims at reducing the total traffic related fatalities and injuries to zero by promoting the adoption of safe mobility design and policies.[^2]

<br>

### Smart Growth America- National Complete Streets Coalition Safety Video

<iframe 
  src="https://www.youtube.com/embed/Ii4fXlCNh8k" 
  frameborder="0" 
  allowfullscreen="true" 
  width= "50%"
  height= "25%"
>
</iframe>


[^1]: Smart Growth America. (2020). _Creating safer streets (fact sheet)._ Retrieved from https://smartgrowthamerica.org/program/national-complete-streets-coalition/resources/

[^2]: Vision Zero Network. (2022). _What is Vision Zero?_ Retrieved November 2022 from https://visionzeronetwork.org/about/what-is-vision-zero/


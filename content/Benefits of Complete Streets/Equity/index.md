---
title: "Equity"
draft: false
weight: 17
---

{{<image src="ethnicity.png" 
  link="https://smartgrowthamerica.org/dangerous-by-design/"
  alt="Photo shows graph of pedestrian death by ethnicity"
  attr="Pedestrian fatalities by race or ethnic group 2016-2020" 
  position="right">}}
 
  
Equity can be defined as “the consistent and systematic fair, just, and impartial treatment of all individuals, including individuals who belong to underserved communities that have been denied such treatment.” [^1] The Complete Streets approach to street design strengthens connectivity across the urbanized area by increasing accessibility for diverse communities using various modes of transportation across the street network. 

{{<image src="income.png" 
  link="https://smartgrowthamerica.org/dangerous-by-design/"
  alt="Photo shows graph of pedestrian death by income"
  attr="Pedestrian fatalities by income group 2016-2020" 
  position="right">}}
  
Underserved populations, low income communities and people of color are disproportionately impacted by transportation emissions and pollution, as well as have less opportunity or access to utilize active transportation networks. [^2] The lack of access to safe active infrastructure creates dangerous travel conditions, contributing to higher risk of pedestrian injury or death. [^3]



[^1]: White House. (2021 January 21). _Executive Order on Advancing Racial Equity and Support for Underserved Communities Through the Federal Government._ Retrieved 21 October 2022 from https://www.whitehouse.gov/briefing-room/presidential-actions/2021/01/20/executive-order-advancing-racial-equity-and-support-for-underserved-communities-through-the-federal-government/

[^2]: U.S. Department of Transportation. (2022). _Complete Streets in FHWA._ Retrieved 21 October 2022 from https://highways.dot.gov/complete-streets/complete-streets-fhwa

[^3]: Smart Growth America. (2022). _Dangerous by Design 2022._ Retrieved December 2022 from https://smartgrowthamerica.org/dangerous-by-design/

However, Complete Streets can be used to create a more equitable streetscape. Complete Streets prioritize instituting safe, accessible networks to all communities, especially those who have been historically underserved or disproportionately impacted by the negative effects of existing transportation systems.  Advancing equity using Complete Streets will look different for each community/urbanized area, depending on each one’s unique needs. Examples of actions include integrating equitable implementation of Complete Streets projects within the community, performing an equity assessment, and identifying gaps in access and/or quality within the existing network.





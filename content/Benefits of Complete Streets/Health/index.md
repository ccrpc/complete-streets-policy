---
title: "Health"
draft: false
weight: 16
---

{{<image src="Yankee Ridge.jpg" 
  link="/destination"
  alt="Photo shows adults and children biking on the sidewalk and using the crosswalk."
  attr="Children biking to Yankee Ridge Elementary in Urbana" 
  position="right">}}
  
Incomplete streets favor vehicles over other modes of transportation, making it feel unsafe to navigate the street as a pedestrian. This impacts people’s health, as pedestrians may feel less inclined to utilize the roadway for travel or a means for physical activity. With over 50% of trips made in 2021 falling under 3 miles (with 24% under 1 mile) ensuring safe short trips is important.[^1]


With complete streets, short trips are safer and more accessible to pedestrians traveling on foot. The Complete Streets approach to design puts people’s needs and wellbeing before vehicles, ensuring that our networks are safe and accessible to all users. These streets encourage pedestrian and cyclist usage across the street networks, increasing movement and physical activity among users. In turn, Complete Streets offer many health benefits, including improving the safety of all users, increasing physical activity and exercise, reducing risk of injuries and fatalities, and reducing transportation related emissions by decreasing dependence on motor vehicles for travel.[^2]

{{<image src="CDC.png" 
  link="https://www.cdc.gov/physicalactivity/activepeoplehealthynation/index.html" width= "25%" height= "25%"
  alt="Photo shows the Active People, Healthy Nation logo."
   position="right">}}
  
The CDC nationwide initiative _Active People, Healthy Nation_ aims to "help 27 million Americans become more physically active by 2027" by "providing equitable and inclusive access to safe places for physical activity."[^3] Initiating Complete Streets in our community advances this goal and promotes a active lifestyle. See the _Active People, Healthy Nation_ webpage for Transportation focused [Tools for Action](https://www.cdc.gov/physicalactivity/activepeoplehealthynation/everyone-can-be-involved/transportation.html). 

### Benefits of Physical Activity

{{<image src="physical activity.png" 
  link="https://www.cdc.gov/physicalactivity/about-physical-activity/pdfs/healthy-strong-america-201902_508.pdf" width= "100%"
  alt="Photo shows benefits of physical activity"
  attr="Source: Center for Disease Control- Physical Activity Builds a Healthy and Strong America" 
   position="bottom">}}



[^1]: U.S. Department of Energy. (2022). _More than Half of all Daily Trips Were Less than Three Miles in 2021._ Office of Energy Efficiency & Renewable Energy. Retrieved from https://www.energy.gov/eere/vehicles/articles/fotw-1230-march-21-2022-more-half-all-daily-trips-were-less-three-miles-2021


[^2]: U.S. Department of Transportation. (2015). _Complete Streets._ Retrieved 21 October 2022 from <https://www.transportation.gov/mission/health/complete-streets>


[^3]: CDC. (2022 June 3). _About Active People, Healthy Nation._ Retrieved 8 December 2022 from https://www.cdc.gov/physicalactivity/activepeoplehealthynation/about-active-people-healthy-nation.html

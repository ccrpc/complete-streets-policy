---
title: "Age and Disability"
draft: false
weight: 20
---

Complete Streets increase network connectivity and thus improve mobility for the aging population and people with disabilities by offering more accessible and safe options to travel. As people age, their speed of travel declines, making them more at risk of serious injury while crossing lengthy intersections and stretches of roadway.[^1]  

Additionally, persons with disabilities may require additional time to cross roadways. [^2] Implementing complete street measures such as a median/refuge allows pedestrians a safe place to rest while waiting until they can continue to cross. Other benefits of complete streets for the aging population and people with disabilities include the following:

-	Visible/clearly marked, wide crosswalks;
-	Curb cuts and ramps;
-	Accessible pedestrian signals and push buttons (including audible signals);
-	Truncated dome/tactile sidewalks;
-	Smooth paved walkways;
-	Accessible public transit stops;
-	Traffic calming measures;
-	Improved lighting, signage, markings

[^1]: National Complete Streets Coalition. (n.d.). _Complete Streets Improve Mobility for Older Americans._ Smart Growth America. Retrieved November 2022 from Complete Streets-Older Americans

[^2]: Gervais, Z. (n.d.). _Accessibility Toolkit: When complete streets help people with disabilities._ Inclusive City Maker. Retrieved November 2022 from When Complete Streets Help People with Disabilities (inclusivecitymaker.com)

{{<image src="age_disability_exs.png" 
  link="/destination"
  alt="Visual examples of complete street elements that benefit the aging population and people with disabilities"
  attr="Visual examples of complete street elements that benefit the aging population and people with disabilities"
  position="bottom">}} [^3]


[^3]: Image Sources (top left) CCRPC; (top right) [Strong Towns](https://www.strongtowns.org/journal/2015/3/2/complete-street-with-inclusive-design); (bottom left) [Kenneth Chan/Daily Hive](https://dailyhive.com/vancouver/vancouver-accessible-pedestrian-signals-push-buttons); (bottom right) [Smart Growth America](https://smartgrowthamerica.org/the-complete-streets-act-is-back/)

<br>



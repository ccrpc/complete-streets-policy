---
title: "Benefits of Complete Streets"
draft: false
menu: main
weight: 20
---

Complete Streets enhance the transportation experience by providing people additional choices for how they will travel to work, school, and other common destinations.  A comprehensive Complete Streets policy has many benefits for  the Champaign-Urbana Urbanized Area that build on the individual, non-motorized transportation policies of the federal government. These benefits include, but are not limited to, the following:


---
title: "Economy"
draft: false
weight: 18
---
{{<image src="MTD Bus Downtown.jpg" 
  link="/destination"
  alt="Photo shows MTD Bus in Downtown Champaign"
  attr="MTD Bus in Downtown Champaign" 
  position="right">}}

Complete Streets strengthen comprehensive transportation systems by providing convenient access to destinations like grocery stores, restaurants and other important commercial areas by travelers using various modes of transportation.  This increased access encourages economic growth and stability by giving people a choice in how and where they commute to and from their destinations. 

{{<image src="commute.png" 
  alt="Photo shows 2020 cenus data commuting patterns in the C-U Area"
  attr="Source: US Census Bureau, 2020 ACS 5 Year Estimates, Table S0801" 
  position="right">}}

In the Champaign-Urbana area, approximately 79% of workers commute to work by personal automobile, 5% by public transportation, 2% by biking and walking, and 1% by other methods ([U.S. Census Bureau, 2020](https://data.census.gov/table?t=Commuting&g=310XX00US16580&tid=ACSST5Y2020.S0801)). By implementing Complete Streets, alternative modes of travel become more accessible and allow users access to more places along their route to and from work (i.e. commercial districts such as downtown, local shops and restaurants).

Moreover, the choice to use alternatives to automobile transportation when gasoline prices are unstable can lower the cost of living for residents and allow people to use their discretionary income for purposes other than transportation.  Integrating alternative, active transportation infrastructure into the design of roadways also has the potential to save local government money by preventing expensive retrofits or costly maintenance down the road.  In this way, community members and governments will have extra income to invest in the local and regional economy.  





---
title: "Sustainability"
draft: false
weight: 19
---

The use of transit, bicycle, and pedestrian transportation modes generate low to no greenhouse gas emissions, which can improve air and water quality in a region.  The increased use of these forms of transportation reduces greenhouse gas emissions from the transportation sector, as well as helps to mitigate the impacts of nonpoint source pollution and climate change.  By improving environmental quality over time, wildlife habitats like air, water, soil, and trees will also recover.  Complete Streets policies provide the opportunity to include provisions for green streets regarding natural elements, such as tree-lined streets or vegetated medians to help reduce stormwater runoff and increase green space. These Complete Street elements enhance green infrastructure and the local quality of life for people in the community.    

{{<image src="UIUC Campus.jpg"
  link="/destination"
  alt="Person biking in a on-street bike lane near UIUC campus"
  attr="Bike Lane on UIUC Campus" 
  position="bottom">}}





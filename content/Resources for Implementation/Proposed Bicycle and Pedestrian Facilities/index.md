---
title: "Proposed Bicycle and Pedestrian Facilites"
draft: false
weight: 30
---

### Urbana Pedestrian Master Plan (2020)

{{<image src="Urbana Ped Master Plan_2020.jpg"
  link="https://ccrpc.org/documents/2020-urbana-pedestrian-master-plan-final-report/"
  alt="Cover of Urbana Pedestrian Master Plan"
  attr="Urbana Pedestrian Master Plan" attrlink="https://ccrpc.org/documents/2020-urbana-pedestrian-master-plan-final-report/"
  position="right">}}

The 2020 City of Urbana Pedestrian Master Plan (UPMP) has the purpose of helping the City of Urbana to become a more economically vibrant, healthy, and sustainable community by identifying steps to make Urbana more walkable. The plan establishes the policies, programs, and projects that will further enhance pedestrian safety, comfort, and accessibility in all of Urbana’s neighborhoods. View the plan [here](https://ccrpc.org/documents/2020-urbana-pedestrian-master-plan-final-report/). Plan Recommendations are found in Chapter 6.

### Urbana Park District Trails Master Plan (2016)

{{<image src="Urbana Park District Trails Master Plan.jpg"
  link="https://ccrpc.org/documents/urbana-park-district-trails-master-plan/"
  alt="Cover of Urbana Park District Master Plan"
  attr="Urbana Park District Trails Master Plan" attrlink="https://ccrpc.org/documents/urbana-park-district-trails-master-plan/"
  position="right">}}

CCRPC developed a Trails Master Plan for the Urbana Park District. This plan is an important step toward completing a city and region-wide greenways and trails system that can be used and enjoyed by residents and visitors alike. More details are available on the project [website](https://updtrails.cuuats.org/documents/), with Plan Recommendations found in Chapter 8.

### Urbana Bicycle Master Plan (2016)

The City of Urbana partnered with the Champaign County Regional Planning Commission and League of Illinois Bicyclists to create a Bicycle Master Plan. The strategic plan set out to make Urbana a more friendly community for bicyclists who enjoy riding recreationally or for transportation. View the plan [here](https://ccrpc.org/documents/urbana-bicycle-master-plan-2016/). Plan Recommendations are found in Chapter 11.

### Champaign Park District Trails Master Plan (2017)

{{<image src="Champaign Parks District Trails.jpg"
  link="https://ccrpc.org/documents/champaign-park-district-trails-master-plan/"
  alt="Cover of Champaign Park District Trails Master Plan"
  attr="Champaign Park District Master Plan" attrlink="https://ccrpc.org/documents/champaign-park-district-trails-master-plan/"
  position="right">}}

The Champaign Park District contracted with the CCRPC to create a trail master plan for Champaign’s parks. The Champaign Park District Trails Master Plan (CPD TMP) takes into account information from the Champaign Park District Strategic Plan 2020, Champaign County Greenways & Trails Plan 2014, 2011 Champaign Trails Plan, and other local and regional active transportation plans. The plan guides Champaign Park District decisions on trails within its jurisdiction and strives to coordinate their efforts with those of surrounding park districts and municipalities to obtain a more integrated, better connected trail system on a regional scale. View the plan [here](https://ccrpc.org/documents/champaign-park-district-trails-master-plan/). Plan Recommendations are found in Chapter 7.

### Walk Champaign Pedestrian Plan (2014)

Walk Champaign is a commitment to improving accessibility for all residents, in all parts of the community, no matter their reason for walking. View the plan [here](https://cityofchampaign.sharepoint.com/_layouts/15/guestaccess.aspx?docid=10019e27bebc74448977fe17b79644db1&authkey=AX6vR52INEvuuhLameq3As0)

### Campus Bike Plan (2014)

The Campus Bicycle Plan is the master plan for bicycle improvements on campus. View the plan [here](https://publish.illinois.edu/bikeatillinois/files/2018/08/2014-campus-bicycle-plan.pdf).

### Savoy Bike and Pedestrian Plan (2017)

{{<image src="Savoy Ped and Bike Plan.jpg"
  link="https://ccrpc.org/documents/savoy-bike-pedestrian-plan/"
  alt="Cover of Savoy Bike and Pedestrian Plan"
  attr="Savoy Bike and Pedestrian Plan" attrlink="https://ccrpc.org/documents/savoy-bike-pedestrian-plan/"
  position="right">}}

CCRPC developed a Complete Streets Policy and Bike & Pedestrian Plan for the Village of Savoy. The Savoy Bike & Pedestrian Plan considers the needs of bicyclists and pedestrians and creates a complete transportation network that connects neighborhoods and amenities that enable residents and visitors, of all ages and abilities, multiple alternatives to moving around the Village of Savoy and connecting with surrounding communities. View the plan [here](https://ccrpc.org/documents/savoy-bike-pedestrian-plan/).
Plan Recommendations are found in Chapter 8.

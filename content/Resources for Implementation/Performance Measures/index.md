---
title: "Performance Measures"
draft: false
weight: 20
---

Please refer to the [CCRPC Report Cards](https://data.ccrpc.org/pages/report-cards) for performance measures on various plans involving or pertaining to complete streets. 

- [LRTP 2045 Annual Report Card](https://data.ccrpc.org/pages/lrtp-2045-report-card)

- [LRTP 2040 Annual Report Card](https://data.ccrpc.org/pages/lrtp-2040-annual-report-card)

- [2018 Region 8 HSTP Report Card](https://data.ccrpc.org/pages/2018-region-8-hstp-report-card)

- [Freight Plan Report Card](https://data.ccrpc.org/pages/freight-plan-report-card)

- [Rural Safety Plan Report Card](https://data.ccrpc.org/pages/rural-safety-plan-report-card)

- [Urban Safety Plan Report Card](https://data.ccrpc.org/pages/urban-safety-plan-report-card)

---
title: "Guidelines and Tools"
draft: false
weight: 25
---


### Crosswalk Guidelines

{{<image src="C-U Crosswalk Guidelines.jpg" width= "100" height= "200" 
  link="/destination"
  alt="Cover of the C-U Pedestrian Crossing Enhancement Guidelines"
  attr="Champaign-Urbana Pedestrian Crossing Enhancement Guidelines" attrlink="https://ccrpc.org/documents/crosswalk-guidelines/"
  position="right">}}

The [Champaign-Urbana Pedestrian Crossing Enhancement Guidelines](https://www.ccrpc.org/transportation/urbana_bicycle_and_pedestrian_plans/champaign-urbana_pedestrian_crossing_enhancement_guidelines.php) is intended to serve as a reference guide for CUUATS member agency staff, citizens, and developers when determining the best engineering solutions to pedestrian safety concerns, particularly with regard to the location and design of crosswalks, pedestrian signals, and other elements of pedestrian safety. These guidelines aim to standardize and fairly determine warrants for installation of pedestrian crossing improvements. The Champaign-Urbana Pedestrian Crossing Enhancement Guidelines were approved by the CUUATS Technical and Policy Committees in September 2017.


### Roundabout Guidelines

{{<image src="CUUATS Roundabout Design Guidelines.jpg"
  link="/https://ccrpc.org/documents/roundabout-design-guidelines/"
  alt="Cover of the CUUATS Roundabout Design Guidelines"
  attr="CUUATS Roundabout Design Guidelines" attrlink="https://ccrpc.org/documents/roundabout-design-guidelines/"
  position="right">}}
  
The objective of the [CUUATS Roundabout Design Guidelines](https://www.ccrpc.org/transportation/roundabout_design_guidelines.php) is to provide CUUATS member agencies with guidelines for the implementation of roundabouts as an alternative to other intersection traffic controls. A modern roundabout is a circular intersection where traffic flows around a center island. Modern roundabouts are defined by two basic operational and design principles: yield-at-entry and deflection for entering traffic. Approved 2012.


### Access Management Guidelines

The purpose of the regional [Access Management Guidelines](https://www.ccrpc.org/transportation/access_management_guidelines.php) is to provide techniques that CUUATS member agencies can use to control access between the roadways and the adjacent land uses in the Champaign-Urbana urbanized area, while preserving the functional integrity of the local roadway system in terms of safety, capacity, and speed. Approved 2013.


### Sustainable Neighborhoods Toolkit
Developed with support from the Illinois Department of Transportation, the [Sustainable Neighborhoods Toolkit](https://www.ccrpc.org/planning/sustainable_neighborhoods_toolkit.php) brings together data and analysis from across the CUUATS modeling suite to analyze neighborhood-level mobility, accessibility, and health in Champaign County. The results from the toolkit are published in the Access Score web application, which can be found [here](https://ccrpc.gitlab.io/access-score/#map=12/40.11032/-88.22888).

<figure class="video_container">
<iframe src="https://ccrpc.gitlab.io/access-score/#map=12/40.11032/-88.22888" frameborder="0" width="900" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>


<!-- blank line -->
----
<!-- blank line -->

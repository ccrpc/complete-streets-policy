---
title: "Additional Resources"
draft: false
weight: 50
---


### Traffic Volumes

For the latest traffic count volumes, please refer to the Illinois Department of Transportation’s (IDOT) Getting Around Illinois [website](https://www.gettingaroundillinois.com/TrafficCounts/index.html).

### Crash Dashboard
The [Champaign County Traffic Crash Dashboard](https://crashdashboard.ccrpc.org/) is an interactive web application that presents traffic crash statistics and maps for Champaign County. It allows you to view data by agency as well as type of crash (i.e. pedestrian, automobile, bicycle). 


### Land Use and Population Projections

<p float="top">
  <img src="Chart_Projected Population in CC.png" width="100%" height= "50%"/>
</p>

More information about land use modeling and population projects can be found on the [Long Range Transportation Plan 2045 Modeling website](https://ccrpc.gitlab.io/lrtp2045/vision/model/).


### Traffic Demand Model

The Champaign County Travel Demand Model [(TDM)](https://ccrpc.gitlab.io/lrtp2045/data/tdm/) is a transportation planning tool developed to evaluate the existing transportation system and forecast future travel demand in the region. 






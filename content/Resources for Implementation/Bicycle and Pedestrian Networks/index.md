---
title: "Bicycle and Pedestrian Networks"
draft: false
weight: 35
---


### Sidewalk Explorer
The [Sidewalk Network Inventory and Assessment](https://ccrpc.gitlab.io/sidewalk-explorer/) is an effort to create a comprehensive database of sidewalk network features within the Champaign Urbana Urbanized Area and surrounding communities. The database is designed to assess and track the condition and Americans with Disabilities Act (ADA) compliance of the sidewalk network, and to highlight potential improvements, such as closure of sidewalk gaps and replacement of non-compliant curb ramps.

<figure class="video_container">
<iframe src="https://ccrpc.gitlab.io/sidewalk-explorer/" frameborder="0" width="900" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>



<br>

### Pedestrian and Bike Counts
Champaign County pedestrian and bike traffic counts can be found on the [Champaign County Regional Data Portal](https://data.ccrpc.org/dataset/traffic_counts). Click “Explore – Preview” to view a map of these counts, and click “Explore – Download” to download a geospatial file of these counts.


### Bike Network
Visit the Bike Illinois interactive [Biking Map](https://bike.illinois.edu/maps/biking-map/).


Map of the current bikeways and trails in the Champaign-Urbana Urbanized Area. [Available here](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjdjISK9Jz7AhXhI30KHVpBBQkQFnoECAoQAQ&url=https%3A%2F%2Fccrpc.org%2Fwp-content%2Fuploads%2F2016%2F11%2FChampaign-Urbana-Savoy-Greenways-Trails-Map-2018-12-21-FINAL-to-Website.pdf&usg=AOvVaw2tW712yHOWF8jZM7wlZVSL).



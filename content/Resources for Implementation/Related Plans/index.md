---
title: "Related Transportation Plans"
draft: false
weight: 45
---


<p float="left">
  <img src="urbana.jpg" width="100%" height= "50%"/>
</p>

### Urbana Plans

**Urbana Pedestrian Master Plan (2020)**

The 2020 City of [Urbana Pedestrian Master Plan](https://ccrpc.org/documents/2020-urbana-pedestrian-master-plan-final-report/) has the purpose of helping the City of Urbana to become a more economically vibrant, healthy, and sustainable community by identifying steps to make Urbana more walkable. The plan establishes the policies, programs, and projects that further enhance pedestrian safety, comfort, and accessibility in all of Urbana’s neighborhoods.

<br>


**Urbana Park District Trails Master Plan (2016)**

CCRPC developed a [Trails Master Plan](https://ccrpc.org/documents/urbana-park-district-trails-master-plan/) for the Urbana Park District. This plan is an important step toward completing a city- and region-wide greenways and trails system that can be used and enjoyed by residents and visitors alike.

<br>

**Urbana Bicycle Master Plan (2016)**

The 2016 [Urbana Bicycle Master Plan](https://ccrpc.org/documents/urbana-bicycle-master-plan-2016/) is a comprehensive update of the 2008 Urbana Bicycle Master Plan, completed by CUUATS, and builds upon the facilities and programs that were identified in the 2008 Plan.

<br>



<p float="left">
  <img src="bike sign.JPG" width="100%" height= "50%"/>
</p>


### CUUATS Plans

**Long Range Transportation Plans**

The Long Range Transportation Plan (LRTP) is a federally mandated document that is updated every five years, and looks at the projected evolution of pedestrian, bicycle, transit, automobile, rail, and air travel over the next 25 years. The LRTP covers a 25-year Metropolitan Planning Area (MPA), which encompasses the Champaign-Urbana Urbanized Area as delineated by the most recent U.S. Census. Its completion permits CUUATS member agencies to receive federal and state funding for transportation projects and programs.

- [LRTP 2045](https://ccrpc.gitlab.io/lrtp2045/)

- [LRTP: Sustainable Choices 2040](https://ccrpc.org/documents/lrtp-sustainable-choices-2040/)

- [LRTP: Choices 2035](https://ccrpc.org/documents/lrtp-choices-2035/)

- [LRTP 2025](https://ccrpc.org/documents/lrtp-2025/)



<br>

**Rural and Urban Safety Plans**

The [Rural and Urban Champaign County Area Safety Plans](https://ccrpc.org/documents/rural-and-urban-safety-plans/) serve as safety guidelines for the area by identifying and implementing safety improvements to reduce fatalities and serious injuries on the roadways. The plans include analyses of crash trends and strategies for approaching safety in priority areas. 


<br>

**C-U Urbanized Area Human Service Transportation Plan (HSTP) (2018)**

The [Human Service Transportation Plan (HSTP)](https://ccrpc.org/documents/2018-champaign-urbana-urbanized-area-human-service-transportation-plan-hstp/) is an initiative to evaluate existing transportation services, identify the transportation needs of individuals with disabilities, older adults, and people with low incomes, and establish goals and strategies for meeting these needs within the Champaign-Urbana Urbanized Area. 

<br>

**C-U Region Freight Plan (2019)**

The [Champaign-Urbana Region Freight Plan](https://ccrpc.org/documents/champaign-urbana-region-freight-plan/) is a project funded by the Illinois Department of Transportation and developed by staff at CUUATS. The goal of the plan is to provide a comprehensive picture of regional freight movements, identify freight stakeholder needs, forecast future freight movement patterns, and propose recommendations that will enhance the movement of both people and goods while mitigating the negative impacts of freight on mobility, safety, environment, and quality of life in the Champaign-Urbana Metropolitan Planning Area (MPA).

<br>

**CUUATS Transit Facility Guidelines (2015)**

The [Champaign-Urbana Urbanized Area Transit Facility Guidelines](https://ccrpc.org/documents/transit-facility-guidelines/) is a comprehensive document that outlines a process to make transit facilities within the urbanized area more accessible for persons with disabilities and lower-income persons. The document includes resources for accessibility and best practices for transit facilities, a multi-level and multi-variable analysis of the CUMTD service area in terms of accessibility and transit facilities, and facility design recommendations for CUMTD to enhance accessibility.

<br>

**Complete Streets Policy (2012)**

The first [CUUATS Complete Street Policy](https://ccrpc.org/documents/complete-streets-policy/) was passed in 2012.

<br>

<p float="left">
  <img src="uiuc crossing.jpg" width="100%" height= "50%"/>
</p>


### Area Plans

**Safe Routes to School Plans**

- [Rantoul Schools SRTS Plan](https://ccrpc.org/documents/rantoul-transportation-and-safe-routes-to-school-plans/)

- [South Side School SRTS Plan](https://ccrpc.org/documents/safe-routes-to-school-south-side/)

- [Prairie Campus SRTS Plan](https://ccrpc.org/documents/safe-routes-to-school-prairie-campus-dr-williams/)

- [Stratton Elementary School SRTS Plan](https://ccrpc.org/documents/safe-routes-to-school-plan-stratton/)

- [Dr. Howard Elementary School SRTS Plan](https://ccrpc.org/documents/dr-howard-safe-routes-to-school-plan/)

- [Thomasboro Grade School SRTS Plan](https://ccrpc.org/documents/safe-routes-to-school-thomasboro/)

<br>

**Champaign Park District Trails Master Plan (2017)**

The [CPD Trails Master Plan](https://ccrpc.org/documents/champaign-park-district-trails-master-plan/) guides Champaign Park District decisions on trails within its jurisdiction and strives to coordinate their efforts with those of surrounding park districts and municipalities to obtain a more integrated, better connected trail system on a regional scale. The plan establishes the policies, programs, and projects that will further enhance the connectivity of area trails for the enjoyment of the residents of the City of Champaign.  

<br>

**Savoy Bike and Pedestrian Plan (2017)**

CCRPC developed a Complete Streets Policy and Bike & Pedestrian Plan for the Village of Savoy. The [Savoy Bike & Pedestrian Plan](https://ccrpc.org/documents/savoy-bike-pedestrian-plan/) considers the needs of bicyclists and pedestrians and creates a complete transportation network that connects neighborhoods and amenities that enable residents and visitors, of all ages and abilities, multiple alternatives to moving around the Village of Savoy and connecting with surrounding communities. 

<br>

**Active Choices: Champaign County Greenways & Trails Plan (2014)**

The [Champaign County Greenways & Trails Plan](https://ccrpc.org/documents/active-choices-champaign-county-greenways-trails-plan-2014/) is an effort coordinated by the Regional Planning Commission with local agencies to develop the county greenways and trails system. In 2011, RPC received a grant from the Illinois Department of Transportation to update the 2004 plan. The update, titled Active Choices, is designed to provide guidance and a framework to ensure Champaign County’s desire to create a bikeable, walkable, and environmentally aware and active community. In June 2022, existing greenways & trails maps for Champaign, Urbana, Savoy, Mahomet, Rantoul, and St. Joseph were updated. The Greenways & Trails maps show off-street trails, on-street bikeways, parks, open spaces, and places of interest in Champaign, Urbana, Savoy, Mahomet, Rantoul, St. Joseph, and Champaign County. 

<br>
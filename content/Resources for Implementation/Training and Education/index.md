---
title: "Trainings and Education"
draft: false
weight: 50
---

<p float="left">
  <img src="biking.jpg" width="100%" height= "50%"/>
</p>

### Trainings

**Designing for Bicyclist Safety (FHWA)**

This course was developed by the Federal Highway Administration (FHWA) and held in Champaign in 2017. Access the training [here](https://www.pedbikeinfo.org/webinars/webinar_details.cfm?id=7).

<br>

### Education

**Complete Streets, Complete Networks (ATA)**

This is the official manual for designing complete streets by the Active Transportation Alliance (ATA) Chicago. Access the manual [here](https://atpolicy.org/resources/design-guides/complete-streets-complete-networks-design-guide/).


**Complete Streets, Complete Networks, Rural Contexts (ATA)**

This is the manual for designing complete streets in the rural context by the Active Transportation Alliance (ATA) Chicago. Access the manual [here](https://mobikefed.org/sites/default/files/complete-streets-in-rural-contexts_995.pdf).


**Public Health Engagement in Complete Streets Initiatives: Examples and Lessons Learned (UIC)**

This study was published by the Institute for Health Research and Policy at the University of Illinois at Chicago in 2019, highlighting how public health agencies and advocates have engaged in Complete Streets inititives in communitites across the United States. Access the study [here](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjk3p_nkvX8AhUQkIkEHWo4CGMQFnoECB0QAQ&url=https%3A%2F%2Fapps.sph.uic.edu%2Fwebdocs%2Fpdf%2FCompleteStreetsReport_508v4sm.pdf&usg=AOvVaw2Xdksbxzha1oPaLLBYnc5n).


**Slow Your Street, A How-to Guide for Pop-up Traffic Calming ([Trailnet](https://trailnet.org/2017/04/24/slow-street-guide-wins-trb-award/))**

This guide provides information on how communities can implement pop-up traffic demonstrations. Access the guide [here](https://drive.google.com/file/d/1b1LlnIRmN9vaHyY-dspcu0aaKVhhEJaV/view).

<br>

### Praise for biking in the C-U Area

**City of Urbana**

- Urbana is a Bicycle Friendly Community, receiving Gold in 2014 and 2018. Access Urbana's Report Card [here](https://bikeleague.org/sites/default/files/bfareportcards/BFC_Fall_2018_ReportCard_Urbana_IL.pdf). The press release for Urbana's 2018 award can be located [here](https://bikeleague.org/content/league-american-bicyclists-announces-61-new-and-renewing-bicycle-friendly-communities).


**City of Champaign**

- Champaign is a Bicycle Friendly Community, receiving Bronze in Fall 2014 and Silver in Fall 2017. Access Champaign's Report Card [here](https://bikeleague.org/sites/default/files/bfareportcards/BFC_Fall_2017_ReportCard_Champaign_IL.pdf).


**Village of Savoy**

- The Village of Savoy received an ITEP Grant in 2021 to construct the First Street Sidepath, connecting Savoy to UIUC Campus. 
  - [WCIA Article](https://www.wcia.com/news/local-news/new-bike-path-connects-savoy-and-ui-campus/)
  - [UIUC Research Park Article](https://researchpark.illinois.edu/article/new-first-street-shared-path-eases-commute-to-research-park/)
  - Village of Savoy [press release](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwi3s5_Gmej7AhWemIkEHXXuCbAQFnoECBAQAQ&url=https%3A%2F%2Fwww.savoy.illinois.gov%2Fvertical%2Fsites%2F%257BD0463038-CAC4-4485-B59A-9F55DCAB155B%257D%2Fuploads%2F2022-10-07_Press_release_First_St_Shared_Use_Path_Ribbon_Ceremony.pdf&usg=AOvVaw0tHNzuUyH2L4zRG7N0trHt) for Ribbon Cutting 


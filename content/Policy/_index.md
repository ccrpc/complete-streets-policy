---
title: Policy
draft: false
menu: main
weight: 30
---

<iframe src="Final_Policy.pdf" width="100%" height="500" frameborder="0" />


## Complete Streets and the Champaign-Urbana Urbanized Area Transportation Study (CUUATS)

A Complete Streets policy for the Champaign-Urbana Urbanized Area Transportation Study (CUUATS) is a sure route towards meeting federal and state mandates to plan for and construct roadways for all users (including motorists, pedestrians, and cyclists of all abilities).  The local Complete Streets Policy also allows CUUATS to establish the reasons for complete streets in the local context, assure collaboration on complete streets principles in the CUUATS area, and explore how these roadways will comply with federal and state mandates.    


While this policy constitutes as the CUUATS Complete Streets policy, Complete Streets policies also exist in the Cities of Champaign  and Urbana,  for the University District, and for the Illinois Department of Transportation. The design for complete streets is not universal since the nature of transportation infrastructure is dependent on the local context.  However, there are a few guiding principles that are useful in the development of a complete streets policy (see below).


### The National Complete Streets Coalition recommends the following elements be included in any Complete Streets policy:  

- Establishes commitment and vision: How and why does the community want to complete its streets? This specifies a clear statement of intent to create a complete, connected network and consider the needs of all users.


- Prioritizes diverse users and include all modes: It prioritizes serving the most vulnerable users and the most underinvested and underserved communities, improving equity.


- Applies to all projects and phases: Instead of a limited set of projects, it applies to all new, retrofit/reconstruction, maintenance, and ongoing projects.


- Allows only clear exceptions: Any exceptions must be specific, with a clear procedure that requires high-level approval and public notice prior to exceptions being granted.


- Mandates coordination: Requires private developers to comply, and interagency coordination between government departments and partner agencies.


- Adopts excellent design guidance: Directs agencies to use the latest and best design criteria and guidelines and sets a time frame for implementing this guidance.


- Requires proactive land-use planning: Considers every project’s greater context, as well as the surrounding community’s current and expected land-use and transportation needs.


- Measures progress: Establishes specific performance measures that match the goals of the broader vision, measurably improve disparities, and are regularly reported to the public.


- Sets criteria for choosing projects: Creates or updates the criteria for choosing transportation projects so that Complete Streets projects are prioritized.


- Creates a plan for implementation: A formal commitment to the Complete Streets approach is only the beginning. It must include specific steps for implementing the policy in ways that will make a measurable impact on what gets built and where.


### The Federal Highway Administration (FHWA) recognizes the following areas of opportunity for Complete Streets planning:

- Improve data collection and analysis to advance safety for all users.


- Support rigorous safety assessment during project development and design to help prioritize safety outcomes across all project types.


- Accelerate adoption of standards and guidance that promote safety and accessibility for all users and support innovation in design.


- Reinforce the primacy of safety for all users in the interpretation of design standards, guidelines, and project review processes.


- Make Complete Streets FHWA’s default approach for funding and designing non-access-controlled roadways. 


### In the process of creating a Complete Streets Policy, the following data may be useful:

- Pedestrian and bicycle crash data [(CCRPC)](https://crashdashboard.ccrpc.org/)


- Pedestrian and bicycle commuter trip data (ACS) and local counts


- The number or percentage of children and adolescents walking or bicycling to school [(SRTS)](http://www.cu-srtsproject.com/)


- Rates of obesity and percent of population that is overweight [(IDPH)](https://dph.illinois.gov/data-statistics.html)


- Population projections and estimates [(CCRPC)](https://ccrpc.gitlab.io/lrtp2045/vision/model/)


- Air pollution data [(IEPA)](https://www2.illinois.gov/epa/topics/air-quality/air-quality-reports/Pages/default.aspx)


- Public input data on active transportation needs/demand ([GT Plan](https://ccrpc.org/documents/active-choices-champaign-county-greenways-trails-plan-2014/) and [LRTP public input](https://ccrpc.gitlab.io/lrtp2045/process/public-involvement/))


- Economic and income data; auto ownership as percentage of income ([Census](https://data.census.gov/), ACS, [BLS - Consumer Expenditure Surveys](https://www.bls.gov/))


- Access to household vehicle ([Census](https://data.census.gov/), ACS)


- Population with disability ([Census](https://data.census.gov/), ACS)


- Language spoken ([Census](https://data.census.gov/), ACS)


- Other socio-demographic data that would best inform equitable practices ([Census](https://data.census.gov/), ACS)


## Connection to Current and Future Planning Efforts
A CUUATS Complete Streets policy will provide a bridge to connect the various research, planning, design, and construction activities undertaken by the MPO.  The Long Range Transportation Plan, Safe Routes to School Plans, CATS Plans, Corridor Studies, and the Greenways and Trails Plan are a few of the planning efforts that will benefit from the clarity and purpose provided by an official CUUATS Complete Streets Policy in regards to transit and non-motorized transportation.  This policy will enhance the collaborative process of deciding how to implement alternative and active transportation in the Champaign-Urbana Urbanized Area.

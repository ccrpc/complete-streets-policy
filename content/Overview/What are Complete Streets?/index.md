---
title: "What are Complete Streets?"
draft: false
weight: 11
---

{{<image src="Diagram of complete streets (NCSC).png"
  alt="Figure shows a diagram of a complete street"
  attr="Complete Street Diagram" attrlink="https://smartgrowthamerica.org/what-are-complete-streets/"
  position="right">}}

***“A Complete Streets approach integrates people and place in the planning, design, construction, operation, and maintenance of our transportation networks. This helps to ensure streets put safety over speed, balance the needs of different modes, and support local land uses, economies, cultures, and natural environments.”***

  _- National Complete Streets Coalition, Smart Growth America_


Complete Streets are avenues, boulevards, roads, and drives with room for every traveler to reach their destinations safely and conveniently.  These types of streets are not simply for automobile use, but provide people across a range of abilities, ages, incomes, and ethnicities a choice to walk, cycle, use public transit or various other modes of travel. Though they may share similar elements, not all complete streets look the same. Examples of complete street elements are bike lanes, bus lanes, sidewalks, among many others (see section below).[^3] 

[^3]: National Complete Streets Coalition. (2022). _What does a Complete Street look like?_ Smart Growth America. Retrieved from https://smartgrowthamerica.org/what-are-complete-streets/


Despite differences in structure, the goal of complete streets remain the same. Complete Streets are planned and constructed for pedestrians, cyclists, motorists, transit riders, and people of diverse socio-demographics to ensure transportation is safe, convenient, and equitable for all people in the community.  

### Elements of Complete Streets

Examples of complete street elements are bike lanes, bus lanes, sidewalks, safe and accessible crosswalks, roundabouts, pedestrian signals, transit stops, and others. Below are some examples of elements within our community. 

{{<image src="Complete street elements.png"
  alt="Figure shows examples of complete street elements in Champaign Urbana"
  attr="Complete Street Elements within Champaign-Urbana" 
  position="bottom">}}


### Implementing Complete Street Elements

<p float="left">
  <img src="incomplete.png" width="100%" />
  <img src="complete.png" width="100%" /> 
</p>
  



---
title: "Introduction"
draft: false
weight: 10
---


{{<image src="Complete streets.png"
  alt="Figure shows a diagram of complete street elements"
  attr="What are complete streets?"
  position="right">}}

The provision of multiple transportation options is an important part of the federal policies that determine regional and local transportation funding decisions.  The Americans with Disabilities Act of 1990 (ADA), the Transportation Equity Act for the 21st Century of 1998 (TEA-21), and the Safe, Accountable, Flexible, Efficient Transportation Equity Act:  A Legacy for Users of 2005 (SAFETEA-LU) together establish a federal mandate for metropolitan planning processes to consider and enhance accommodations for people with disabilities, pedestrians, and bicyclists on public roadways. [^1]   

More recently, the 2021 Bipartisan Infrastructure Law (BIL), also known as the Infrastructure Investment and Jobs Act (IIJA), defines complete streets as those that “ensure the safe and adequate accommodation of all users of the transportation system, including pedestrians, bicyclists, public transportation users, children, older individuals, individuals with disabilities, motorists, and freight vehicles.” This bill requires States and Metropolitan Planning Organizations (MPOs) use 2.5% of their “planning and research funds for Complete Streets activities that will increase safe and accessible transportation options.” [^2] Thus, the CUUATS Complete Streets policy is an important piece to advancing complete street strategies within the community.

[^1]: U.S. DOT FHWA. _Bicycle Transportation and Pedestrian Walkways: Legislative and Policy History._ Bicycle & Pedestrian Program. Web. 19 July 2012. <http://www.fhwa.dot.gov/environment/bicycle_pedestrian/legislation/history_legpol.cfm>.


[^2]: FHWA. (2022). _Making to a complete streets design model: A report to Congress on opportunities and challenges._ Retrieved from <https://highways.dot.gov/sites/fhwa.dot.gov/files/2022-03/Complete Streets Report to Congress.pdf>

Established in 2004 by the National Complete Streets Coalition, the Complete Streets movement is helping to leverage federal policies into a transformation in how local and state agencies plan and construct transportation facilities across the United States. In 2007, the Illinois Department of Transportation (IDOT)  adopted a Complete Streets policy in accordance with the Illinois Public Act 095-0665 (Illinois Complete Streets Law).  These policies helped redistribute state and local transportation priorities and investments towards non-motorized transportation infrastructure. 

In 2012, CUUATS passed its first Complete Streets Policy. Local agencies have also developed and approved their own complete streets policies: the City of Urbana  their Comprehensive Plan 2005; the City of Champaign adopted a Complete Streets policy as part of Champaign Tomorrow: 2011 Comprehensive Plan; the Village of Savoy adopted a Complete Streets Policy in 2017; the University of Illinois, Cities of Champaign and Urbana, and the Champaign-Urbana Mass Transit District (MTD) have adopted Complete Streets Principles for the Campus Area Transportation Study. Local agencies have also developed and approved bicycle and pedestrian plans: The City of Champaign updated its Bicycle Vision Network Plan in 2022; the City of Urbana updated its Bicycle Master Plan in 2016 and the Pedestrian Master Plan in 2020; the University of Illinois developed its Campus Bicycle Master Plan in 2014. 

An overarching policy framework like the Complete Streets policy allows members of the Champaign-Urbana Metropolitan Planning Organization (MPO) to address our comprehensive transportation system by formulating local priorities for complete streets and considering their connection to federal and state policies.  



